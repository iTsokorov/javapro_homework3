package com.hillel.tsokorov.homework3;

public class Main {
    public static void main(String[] args) {

        Employee employee = new Employee("Tsokorov Pavlo Dmytrovych",
                "Java Developer",
                "pavlotsokorov@gmail.com",
                "+380962308010",
                22);

        Car car = new Car();
        car.start();

    }
}
