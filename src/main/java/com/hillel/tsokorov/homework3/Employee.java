package com.hillel.tsokorov.homework3;

public class Employee {
    private final String FULL_NAME;
    private final String POSITION;
    private final String EMAIL;
    private final String PHONE_NUMBER;
    private final int AGE;

    public Employee(String FULL_NAME, String POSITION, String EMAIL, String PHONE_NUMBER, int AGE) {
        this.FULL_NAME = FULL_NAME;
        this.POSITION = POSITION;
        this.EMAIL = EMAIL;
        this.PHONE_NUMBER = PHONE_NUMBER;
        this.AGE = AGE;
    }

    public String getFULL_NAME() {
        return FULL_NAME;
    }

    public String getPOSITION() {
        return POSITION;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public String getPHONE_NUMBER() {
        return PHONE_NUMBER;
    }

    public int getAGE() {
        return AGE;
    }
}
